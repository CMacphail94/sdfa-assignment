/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "Sequence.h"
#include "Filter.h"
#include "TriOscillator.h"
#include "SquareOscillator.h"
#include "SawOscillator.h"


class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback

{
public:
    /** Audio Constructor */
    Audio();
    
    /** Audio Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    /** Returns the filter, don't keep a copy of it! */
    Filter& getFilter() { return filter;}
    
    /** Function for dealing with incoming MIDI */
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    /** Handles audio sample by sample, in and out */
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    /** Called when audio device is about to start */
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    /** Called when audio device is stopped */
    void audioDeviceStopped() override;
    
    /** Sends frequency and amplitude info from sequencer to oscillator - plays a step */
    void playStep(float freq, float amp, float detune);
    
    /** Sets sequence length */
    void setStepLength(int length);
    
    /** Set Oscillator mix values */
    void setMixVal(float a, float b);
    
    
    
private:
    AudioDeviceManager audioDeviceManager;
    Filter filter;
    float amplitude;
    float frequency;
    float phasePosition;
    float sampleRate;
    CriticalSection sharedMemory;
    SawOscillator oscillator, oscillator1;
    SquareOscillator oscillator2, oscillator3;
    int timerCount;
    int stepLength;
    float mixVal, mixVal1;
    bool filterBypass;
    
    
};



#endif  // AUDIO_H_INCLUDED
