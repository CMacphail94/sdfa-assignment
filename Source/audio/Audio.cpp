/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio ()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    timerCount = 0;
    stepLength = 100; //set default stepLength
    mixVal = 0.5;
    mixVal1 = 0.5; // set default oscillator mix ( half of each)
    filterBypass = false;
    
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    
    //All MIDI inputs arrive here
    if(message.isNoteOn())
    {
        sharedMemory.enter();
        amplitude = 1.0;
        oscillator.setAmplitude(amplitude);
        frequency = juce::MidiMessage::getMidiNoteInHertz(message.getNoteNumber());
        oscillator.setFrequency(frequency);
        sharedMemory.exit();
    }
    
    if(message.isNoteOff())
    {
        sharedMemory.enter();
        amplitude = 0.0;
        oscillator.setAmplitude(amplitude);
        sharedMemory.exit();
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    float triMix, sqrMix;
    
    filterBypass = filter.getBypassState();
    

    while(numSamples--)
    {
        triMix = (oscillator.nextSample()/2.0) + (oscillator1.nextSample()/2.0);
        sqrMix = (oscillator2.nextSample()/2.0) + (oscillator3.nextSample()/2.0);
        
        float mix = (sqrMix * mixVal) + (triMix * mixVal1);
        
        if(filterBypass == true)
        {
            *outL = mix;
            *outR = mix;
        }
        else
        {
            *outL = filter.Process(mix);
            *outR = filter.Process(mix);
        }
        
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
    
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    
    
}



void Audio::audioDeviceStopped()
{

}

void Audio::playStep(float freq, float amp, float detune)
{
    uint32 time = Time::getMillisecondCounter();
    oscillator.setFrequency(freq);
    oscillator1.setFrequency(freq + detune);
    oscillator2.setFrequency(freq);
    oscillator3.setFrequency(freq + detune);
    Time::waitForMillisecondCounter(time + 2);
    oscillator.setAmplitude(amp/4);
    oscillator1.setAmplitude(amp/4);
    oscillator2.setAmplitude(amp/4);
    oscillator3.setAmplitude(amp/4);
    Time::waitForMillisecondCounter(time + 4);
    oscillator.setAmplitude(amp/2);
    oscillator1.setAmplitude(amp/2);
    oscillator2.setAmplitude(amp/2);
    oscillator3.setAmplitude(amp/2);
    Time::waitForMillisecondCounter(time + 6);
    oscillator.setAmplitude(amp);
    oscillator1.setAmplitude(amp);
    oscillator2.setAmplitude(amp);
    oscillator3.setAmplitude(amp);
    Time::waitForMillisecondCounter((time + stepLength)/2.0);
    oscillator.setAmplitude(amp/2);
    oscillator1.setAmplitude(amp/2);
    oscillator2.setAmplitude(amp/2);
    oscillator3.setAmplitude(amp/2);
    Time::waitForMillisecondCounter(time + stepLength);
    oscillator.setAmplitude(0.0);
    oscillator1.setAmplitude(0.0);
    oscillator2.setAmplitude(0.0);
    oscillator3.setAmplitude(0.0);
    
    
}

void Audio::setStepLength(int length)
{
    stepLength = length;
}

void Audio::setMixVal(float a,float b)
{
    mixVal = a;
    mixVal1 = b;
}



