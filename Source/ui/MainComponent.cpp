/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"



//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_) , filterUI(audio_.getFilter())
{
    setSize (820, 600);

    sequencer.setBounds(10, 20 , 800, 300);
    addAndMakeVisible(sequencer);
    
    filterUI.setBounds(10, 320 , 300, 240);
    addAndMakeVisible(filterUI);
    
    startButton.setBounds(530,375, 60,25);
    startButton.setButtonText("Start");
    startButton.addListener(this);
    addAndMakeVisible(startButton);
    
    tempoSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
    tempoSlider.setBounds(545, 425, 30, 30);
    tempoSlider.setRange(20, 500);
    tempoSlider.setTextBoxStyle(juce::Slider::NoTextBox,true,0,0);
    tempoSlider.addListener(this);
    addAndMakeVisible(tempoSlider);
    
    tempoLabel.setBounds(535, 455, 50, 20);
    tempoLabel.setText("Speed",dontSendNotification);
    addAndMakeVisible(tempoLabel);
    
    stepLengthSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
    stepLengthSlider.setBounds(635, 425 , 30, 30);
    stepLengthSlider.setRange(20,200);
    stepLengthSlider.setTextBoxStyle(juce::Slider::NoTextBox,true,0,0);
    stepLengthSlider.addListener(this);
    addAndMakeVisible(stepLengthSlider);
    
    stepLengthLabel.setBounds(615, 455, 70, 20);
    stepLengthLabel.setText("Step Length",dontSendNotification);
    addAndMakeVisible(stepLengthLabel);
    
    detuneSlider.setSliderStyle(juce::Slider::RotaryVerticalDrag);
    detuneSlider.setBounds(455, 425, 30, 30);
    detuneSlider.setRange(0,2.0);
    detuneSlider.setTextBoxStyle(juce::Slider::NoTextBox,true,0,0);
    detuneSlider.addListener(this);
    addAndMakeVisible(detuneSlider);
    
    detuneLabel.setBounds(445, 455, 50 , 20);
    detuneLabel.setText("Detune",dontSendNotification);
    addAndMakeVisible(detuneLabel);
    
    oscMixSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    oscMixSlider.setBounds(480, 500, 150, 30);
    oscMixSlider.setRange(0.0,1,0);
    oscMixSlider.setTextBoxStyle(juce::Slider::NoTextBox,true,0,0);
    oscMixSlider.addListener(this);
    addAndMakeVisible(oscMixSlider);
    
    oscMixLabel.setBounds(480, 530, 150 , 20);
    oscMixLabel.setText("Osc Mix - Square - Saw",dontSendNotification);
    addAndMakeVisible(oscMixLabel);
    
    
    start = false;
    
    counter.setListener(this);
    //Filter();

}

MainComponent::~MainComponent()
{
    counter.stopThread(500);
}

void MainComponent::resized()
{
    
}

void MainComponent::buttonClicked(juce::Button *button)
{
    
    if(start == false)
    {
        start = true;
        counter.startThread();
        button->setButtonText("Stop");
    }
    else{
        start = false;
        counter.stopThread(500);
        button->setButtonText("Start");
    }
    
    
    
    
}

void MainComponent::counterChanged(const unsigned int counterValue)
{
    std::cout <<"\n Step "<< counterValue << "\n";
    float amp = sequencer.getStepAmp(counterValue);
    float freq = sequencer.getStepFreq(counterValue);
    audio.playStep(freq, amp, detune);
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    if(slider == &tempoSlider)
    {
        counter.setTempo(slider->getValue());
    }
    else if(slider == &stepLengthSlider)
    {
        audio.setStepLength(slider->getValue());
    }
    else if(slider == &oscMixSlider)
    {
        float x,y;
        x = 1 - slider->getValue();
        y = 0 + slider->getValue();
        
        audio.setMixVal(x,y);
    }
    else
    {
        detune = slider->getValue();
    }
    
}

void MainComponent::paint (Graphics& g)
{
    g.setColour(juce::Colours::yellowgreen);
    g.fillRect(310,320,500,240);
    
}





//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

