/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"
#include "Sequence.h"
#include "Counter.h"
#include "FilterUI.h"



//==============================================================================
/**
  *  This component lives inside our window, and this is where all controls and content live.
  *  
  *
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Button::Listener,
                        public Counter::Listener,
                        public Slider::Listener

{
public:
    //==============================================================================
    /** Main Component Constructor */
    MainComponent (Audio& audio_);

    /** Main Component Destructor */
    ~MainComponent();

    /** Called when window resized */
    void resized() override;
    
    /** Called when button clicked */
    void buttonClicked (Button* button) override;
    
    /** Called when counter changes */
    void counterChanged(const unsigned int counterValue) override;
    
    /** Called when slider value changed */
    void sliderValueChanged(Slider* slider) override;
    
    /** Called to paint the component */
    void paint (Graphics& g) override;
  
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
private:
    Audio& audio;
    Slider tempoSlider;
    Label tempoLabel;
    Slider stepLengthSlider;
    Label stepLengthLabel;
    Label oscMixLabel;
    Slider detuneSlider;
    Slider oscMixSlider;
    Label detuneLabel;
    TextButton startButton;
    Sequence sequencer;
    FilterUI filterUI;
    Counter counter;
    bool start;
    float detune;
    
    

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
