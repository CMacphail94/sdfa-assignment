//
//  FilterUI.h
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 25/01/2015.
//
//

#ifndef H_FILTERUI
#define H_FILTERUI

#include "../JuceLibraryCode/JuceHeader.h"
#include "Filter.h"


/**
 * Class for Filter UI
 * @see Filter
 */
class FilterUI : public Component,
                public Slider::Listener,
public Button::Listener

{
public:
    /** Filter Ui Constructor
     * @param &filter_ a copy of the Filter object.
     */
    FilterUI(Filter &filter_);
    
    /** Filter Ui Destructor*/
    ~FilterUI();
    
    /** Called when resized */
    void resized() override;
    
    /** Filter Ui paint 
     * @param g reference to Graphics object.
     */
    void paint (Graphics& g) override;
    
    /** Called when filter ui slider changed 
     * @param slider refers to slider which has changed value.
     */
    void sliderValueChanged (Slider* slider) override;
    
    /** Called when filter ui button pressed 
     * @param button refers to the button which was clicked.
     */
    void buttonClicked (Button* button) override;
    
    
    
private:
    Slider freqCutoffSlider;
    Filter &filter;
    TextButton filterTypeButton;
    TextButton filterBypassButton;
    Label filterLabel;
    

    
    
    
    
    
    
};


#endif //H_FILTERUI
