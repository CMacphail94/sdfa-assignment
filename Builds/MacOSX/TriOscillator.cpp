//
//  TriOscillator.cpp
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 27/12/2014.
//
//

#include "TriOscillator.h"
#include <cmath>

TriOscillator::TriOscillator()
{
    reset();
}
TriOscillator::~TriOscillator()
{
    
}

void TriOscillator::setFrequency (float freq)
{
	frequency = freq;
	phaseInc = (2 * M_PI * frequency ) / sampleRate ;
}

void TriOscillator::setNote (int noteNum)
{
	setFrequency (440.f * pow (2, (noteNum - 69) / 12.0));
}

void TriOscillator::setAmplitude (float amp)
{
	amplitude = amp;
}

void TriOscillator::reset()
{
    phase = 0.f;
    sampleRate = 44100;
    setFrequency (440.f);
	setAmplitude (0.f);
}

void TriOscillator::setSampleRate (float sr)
{
	sampleRate = sr;
	setFrequency (frequency );//just to update the phaseInc
}

float TriOscillator::nextSample()
{
	float out = renderWaveShape (phase ) * amplitude ;
	phase = phase + phaseInc ;
	if(phase  > (2.f * M_PI))
		phase -= (2.f * M_PI);
    
	return out;
}

float TriOscillator::renderWaveShape (const float currentPhase)
{
    
    
    float a,y;
    a = 1.0;
    
    if (currentPhase < M_PI)
    {
        y = -a + (2 * a / M_PI) * currentPhase;
        return y;
    }
    else
    {
        y = 3 * a - (2 * a / M_PI) * currentPhase;
        return y;
    }

}