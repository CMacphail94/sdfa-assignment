//
//  FilterUI.cpp
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 25/01/2015.
//
//

#include "FilterUI.h"

//==============================================================================
FilterUI::FilterUI(Filter &filter_) : filter (filter_)
{
    freqCutoffSlider.setBounds(10, 10 , 50 , 180);
    freqCutoffSlider.setSliderStyle(juce::Slider::LinearVertical);
    freqCutoffSlider.setRange(30,1500);
    freqCutoffSlider.setTextBoxStyle(juce::Slider::NoTextBox,true,0,0);
    freqCutoffSlider.addListener(this);
    addAndMakeVisible(freqCutoffSlider);
    
    filterLabel.setBounds(0, 210, 100, 20);
    filterLabel.setText("Filter",dontSendNotification);
    addAndMakeVisible(filterLabel);
    
    
    filterTypeButton.setBounds(90, 55, 70, 25);
    filterTypeButton.setButtonText("low pass");
    filterTypeButton.addListener(this);
    addAndMakeVisible(filterTypeButton);
    
    filterBypassButton.setBounds(160,55, 70, 25);
    filterBypassButton.setButtonText("bypass");
    filterBypassButton.addListener(this);
    addAndMakeVisible(filterBypassButton);
}


FilterUI::~FilterUI()
{
    
}

void FilterUI::resized()
{
    
    
}

void FilterUI::paint(Graphics& g)
{
    g.fillAll(Colours::orchid);
}

void FilterUI::sliderValueChanged(juce::Slider *slider)
{
    filter.setCutoff(slider->getValue());
}

void FilterUI::buttonClicked(juce::Button *button)
{
   if(button == &filterTypeButton)
   {
       if (button->getButtonText() == "low pass")
       {
           filter.setFilterType(1);
           button->setButtonText("hi pass");
       }
       else if(button->getButtonText() == "hi pass")
       {
           filter.setFilterType(0);
           button->setButtonText("low pass");
       }
   }else
   {
       if (button->getButtonText() == "bypass")
       {
           filter.setBypassState(true);
           button->setButtonText("filter");
       }
       else if(button->getButtonText() == "filter")
       {
           filter.setBypassState(false);
           button->setButtonText("bypass");
       }
   }
}
