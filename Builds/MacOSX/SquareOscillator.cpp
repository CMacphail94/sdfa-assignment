//
//  SawOscillator.cpp
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 27/12/2014.
//
//

#include "SquareOscillator.h"
#include <cmath>

SquareOscillator::SquareOscillator()
{
    reset();
}
SquareOscillator::~SquareOscillator()
{
    
}

void SquareOscillator::setFrequency (float freq)
{
	frequency = freq;
	phaseInc = (2 * M_PI * frequency ) / sampleRate ;
}

void SquareOscillator::setNote (int noteNum)
{
	setFrequency (440.f * pow (2, (noteNum - 69) / 12.0));
}

void SquareOscillator::setAmplitude (float amp)
{
	amplitude = amp;
}

void SquareOscillator::reset()
{
    phase = 0.f;
    sampleRate = 44100;
    setFrequency (440.f);
	setAmplitude (0.f);
}

void SquareOscillator::setSampleRate (float sr)
{
	sampleRate = sr;
	setFrequency (frequency );//just to update the phaseInc
}

float SquareOscillator::nextSample()
{
	float out = renderWaveShape (phase ) * amplitude ;
	phase = phase + phaseInc ;
	if(phase  > (2.f * M_PI))
		phase -= (2.f * M_PI);
    
	return out;
}

float SquareOscillator::renderWaveShape (const float currentPhase)
{
    
    
    if(currentPhase < M_PI && currentPhase > 0)
    {
        return 1.0;
    }else if(currentPhase > M_PI && currentPhase < 2.f * M_PI)
    {
        return (-1.0);
    
    }
    
	return 0;
}