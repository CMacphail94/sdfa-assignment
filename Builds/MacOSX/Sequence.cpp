//
//  Sequence.cpp
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 29/12/2014.
//
//

#include "Sequence.h"


//==============================================================================
Sequence::Sequence()
{
    //UI Component Setup
    for(int i=0;i<8;i++)
    {
     ampSlider[i].setBounds(40 + (100 * i), 20 , 20, 150);
     ampSlider[i].setSliderStyle(juce::Slider::LinearVertical);
     ampSlider[i].setRange(0.0,1.0);
     ampSlider[i].addListener(this);
     addAndMakeVisible(&ampSlider[i]);
        
     seqButton[i].setBounds(40 + (100 * i), 175, 20, 20);
     seqButton[i].setButtonText(" ");
     seqButton[i].addListener(this);
     addAndMakeVisible(&seqButton[i]);
        
        seqValues[i] = 0;
        seqAmps[i] = 0;
    }
    
    sequencerLabel.setBounds(0, 265, 100, 20);
    sequencerLabel.setText("Sequencer",dontSendNotification);
    addAndMakeVisible(sequencerLabel);
    
    rootKeySelect.setBounds(690, 225, 100, 20);
    rootKeySelect.setText("Key Select",dontSendNotification);
    rootKeySelect.addItem("C2",1);
    rootKeySelect.addItem("C#2",2);
    rootKeySelect.addItem("D2",3);
    rootKeySelect.addItem("D#2",4);
    rootKeySelect.addItem("E2",5);
    rootKeySelect.addItem("F2",6);
    rootKeySelect.addItem("F#2",7);
    rootKeySelect.addItem("G2",8);
    rootKeySelect.addItem("G#2",9);
    rootKeySelect.addItem("A2",10);
    rootKeySelect.addItem("Bb2",11);
    rootKeySelect.addItem("B2",12);
    rootKeySelect.addListener(this);
    addAndMakeVisible(rootKeySelect);
    
    rootKeyValue = 55; //A1
    
    
    
    
}

Sequence::~Sequence()
{
    
}

void Sequence::resized()
{
    
    
}

void Sequence::paint(Graphics& g)
{
    g.fillAll(Colours::cornflowerblue);
}


void Sequence::buttonClicked(juce::Button *button)
{
    if(button->getButtonText() == " ")
    {
        button->setButtonText("1");
        
        //Set Sequence Data
        if(button == &seqButton[0])
        {
            seqValues[0] = 1;
        }
        else if(button == &seqButton[1])
        {
            seqValues[1] = 1;
        }
        else if(button == &seqButton[2])
        {
            seqValues[2] = 1;
        }
        else if(button == &seqButton[3])
        {
            seqValues[3] = 1;
        }
        else if(button == &seqButton[4])
        {
            seqValues[4] = 1;
        }
        else if(button == &seqButton[5])
        {
            seqValues[5] = 1;
        }
        else if(button == &seqButton[6])
        {
            seqValues[6] = 1;
        }
        else if(button == &seqButton[7])
        {
            seqValues[7] = 1;
        }
    }
    else if(button->getButtonText() == "1")
    {
        button->setButtonText("2");
        
        if(button == &seqButton[0])
        {
            seqValues[0] = 2;
        }
        else if(button == &seqButton[1])
        {
            seqValues[1] = 2;
        }
        else if(button == &seqButton[2])
        {
            seqValues[2] = 2;
        }
        else if(button == &seqButton[3])
        {
            seqValues[3] = 2;
        }
        else if(button == &seqButton[4])
        {
            seqValues[4] = 2;
        }
        else if(button == &seqButton[5])
        {
            seqValues[5] = 2;
        }
        else if(button == &seqButton[6])
        {
            seqValues[6] = 2;
        }
        else if(button == &seqButton[7])
        {
            seqValues[7] = 2;
        }
    }
    else if(button->getButtonText() == "2")
    {
        button->setButtonText("3");
        
        if(button == &seqButton[0])
        {
            seqValues[0] = 3;
        }
        else if(button == &seqButton[1])
        {
            seqValues[1] = 3;
        }
        else if(button == &seqButton[2])
        {
            seqValues[2] = 3;
        }
        else if(button == &seqButton[3])
        {
            seqValues[3] = 3;
        }
        else if(button == &seqButton[4])
        {
            seqValues[4] = 3;
        }
        else if(button == &seqButton[5])
        {
            seqValues[5] = 3;
        }
        else if(button == &seqButton[6])
        {
            seqValues[6] = 3;
        }
        else if(button == &seqButton[7])
        {
            seqValues[7] = 3;
        }
    }
    else if(button->getButtonText() == "3")
    {
            button->setButtonText("-1");
            
            if(button == &seqButton[0])
            {
                seqValues[0] = -1;
            }
            else if(button == &seqButton[1])
            {
                seqValues[1] = -1;
            }
            else if(button == &seqButton[2])
            {
                seqValues[2] = -1;
            }
            else if(button == &seqButton[3])
            {
                seqValues[3] = -1;
            }
            else if(button == &seqButton[4])
            {
                seqValues[4] = -1;
            }
            else if(button == &seqButton[5])
            {
                seqValues[5] = -1;
            }
            else if(button == &seqButton[6])
            {
                seqValues[6] = -1;
            }
            else if(button == &seqButton[7])
            {
                seqValues[7] = -1;
            }
            


}
    else if(button->getButtonText() == "-1")
    {
        button->setButtonText("-2");
        
        if(button == &seqButton[0])
        {
            seqValues[0] = -2;
        }
        else if(button == &seqButton[1])
        {
            seqValues[1] = -2;
        }
        else if(button == &seqButton[2])
        {
            seqValues[2] = -2;
        }
        else if(button == &seqButton[3])
        {
            seqValues[3] = -2;
        }
        else if(button == &seqButton[4])
        {
            seqValues[4] = -2;
        }
        else if(button == &seqButton[5])
        {
            seqValues[5] = -2;
        }
        else if(button == &seqButton[6])
        {
            seqValues[6] = -2;
        }
        else if(button == &seqButton[7])
        {
            seqValues[7] = -2;
        }
        
    }
    else if(button->getButtonText() == "-2")
    {
        button->setButtonText("-3");
        
        if(button == &seqButton[0])
        {
            seqValues[0] = -3;
        }
        else if(button == &seqButton[1])
        {
            seqValues[1] = -3;
        }
        else if(button == &seqButton[2])
        {
            seqValues[2] = -3;
        }
        else if(button == &seqButton[3])
        {
            seqValues[3] = -3;
        }
        else if(button == &seqButton[4])
        {
            seqValues[4] = -3;
        }
        else if(button == &seqButton[5])
        {
            seqValues[5] = -3;
        }
        else if(button == &seqButton[6])
        {
            seqValues[6] = -3;
        }
        else if(button == &seqButton[7])
        {
            seqValues[7] = -3;
        }
        
    }
    else
    {
        button->setButtonText(" ");
        
        if(button == &seqButton[0])
        {
            seqValues[0] = 0;
        }
        else if(button == &seqButton[1])
        {
            seqValues[1] = 0;
        }
        else if(button == &seqButton[2])
        {
            seqValues[2] = 0;
        }
        else if(button == &seqButton[3])
        {
            seqValues[3] = 0;
        }
        else if(button == &seqButton[4])
        {
            seqValues[4] = 0;
        }
        else if(button == &seqButton[5])
        {
            seqValues[5] = 0;
        }
        else if(button == &seqButton[6])
        {
            seqValues[6] = 0;
        }
        else if(button == &seqButton[7])
        {
            seqValues[7] = 0;
        }
    }

}
void Sequence::sliderValueChanged(juce::Slider *slider)
{
    DBG(">>>>>>");
    if(slider == &ampSlider[0])
    {
        seqAmps[0] = slider->getValue();
    }
    else if(slider == &ampSlider[1])
    {
        seqAmps[1] = slider->getValue();
    }
    else if(slider == &ampSlider[2])
    {
        seqAmps[2] = slider->getValue();
    }
    else if(slider == &ampSlider[3])
    {
        seqAmps[3] = slider->getValue();
    }
    else if(slider == &ampSlider[4])
    {
        seqAmps[4] = slider->getValue();
    }
    else if(slider == &ampSlider[5])
    {
        seqAmps[5] = slider->getValue();
    }
    else if(slider == &ampSlider[6])
    {
        seqAmps[6] = slider->getValue();
    }
    else if(slider == &ampSlider[7])
    {
        seqAmps[7] = slider->getValue();
    }
    
}

void Sequence::comboBoxChanged(juce::ComboBox *comboBox)
{
    if(comboBox->getText() == "C2")
    {
        rootKeyValue = 65.41;
    }
    else if(comboBox->getText() == "C#2")
    {
        rootKeyValue = 69.30;
    }
    else if(comboBox->getText() == "D2")
    {
        rootKeyValue = 73.42;
    }
    else if(comboBox->getText() == "D#2")
    {
        rootKeyValue = 77.78;
    }
    else if(comboBox->getText() == "E2")
    {
        rootKeyValue = 82.41;
    }
    else if(comboBox->getText() == "F2")
    {
        rootKeyValue = 87.31;
    }
    else if(comboBox->getText() == "F#2")
    {
        rootKeyValue = 92.50;
    }
    else if(comboBox->getText() == "G2")
    {
        rootKeyValue = 98.00;
    }
    else if(comboBox->getText() == "G#2")
    {
        rootKeyValue = 103.83;
    }
    else if(comboBox->getText() == "A2")
    {
        rootKeyValue = 110.00;
    }
    else if(comboBox->getText() == "Bb2")
    {
        rootKeyValue = 116.54;
    }
    else if(comboBox->getText() == "B2")
    {
        rootKeyValue = 123.47;
    }
    else
    {
        rootKeyValue = 110.0;//Default to A2
    }
 
}

int Sequence::getStepFreq(int step)
{
    if(seqValues[step] == 0)
    {
        return rootKeyValue;
    }
    else if(seqValues[step] == 1)
    {
        return rootKeyValue * 1.25;
    }
    else if(seqValues[step] == 2)
    {
        return rootKeyValue * 1.50;
    }
    else if(seqValues[step] == 3)
    {
        return rootKeyValue * 1.75;
    }
    else if(seqValues[step] == 4)
    {
        return rootKeyValue * 2.00;
    }
    else if(seqValues[step] == -1)
    {
        return rootKeyValue * 0.75;
    }
    else if(seqValues[step] == -2)
    {
        return rootKeyValue * 0.5;
    }
    else if(seqValues[step] == -3)
    {
        return rootKeyValue * 0.25;
    }
    
    return 0;
    
}

float Sequence::getStepAmp(int step)
{
      
    return seqAmps[step];
}

int Sequence::getStep(int step)
{
    return currentStep;
}

