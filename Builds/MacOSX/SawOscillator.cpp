//
//  SawOscillator.cpp
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 27/12/2014.
//
//

#include "SawOscillator.h"
#include <cmath>

SawOscillator::SawOscillator()
{
    reset();
}
SawOscillator::~SawOscillator()
{
    
}

void SawOscillator::setFrequency (float freq)
{
	frequency = freq;
	phaseInc = (2 * M_PI * frequency ) / sampleRate ;
}

void SawOscillator::setNote (int noteNum)
{
	setFrequency (440.f * pow (2, (noteNum - 69) / 12.0));
}

void SawOscillator::setAmplitude (float amp)
{
	amplitude = amp;
}

void SawOscillator::reset()
{
    phase = 0.f;
    sampleRate = 44100;
    setFrequency (440.f);
	setAmplitude (0.f);
}

void SawOscillator::setSampleRate (float sr)
{
	sampleRate = sr;
	setFrequency (frequency );//just to update the phaseInc
}

float SawOscillator::nextSample()
{
	float out = renderWaveShape (phase ) * amplitude ;
	phase = phase + phaseInc ;
	if(phase  > (2.f * M_PI))
		phase -= (2.f * M_PI);
    
	return out;
}

float SawOscillator::renderWaveShape (const float currentPhase)
{
    
    
    float a,y;
    a = 1.0;
    
    
    y = a - (a / M_PI * currentPhase);
    
    return y;
    
}