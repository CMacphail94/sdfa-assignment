/*
 *  TriOscillator.h
 *
 *
 *
 *
 */


#ifndef H_TRIOSCILLATOR
#define H_TRIOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"

/**
 * Class for a triangle wave oscillator
 * @see SquareOscillator
 * @see SawOscillator
 */

class TriOscillator
{
public:
	//==============================================================================
	/**
	 TriOscillator constructor
	 */
	TriOscillator();
	
	/**
	 TriOscillator destructor
	 */
	~TriOscillator();
	
	/**
	 * sets the frequency of the oscillator
     * @param freq frequency to set oscillator to
	 */
	void setFrequency (float freq);
	
	/**
	 * sets frequency using a midi note number
     * @param noteNum integer value of note number to set oscillator to.
	 */
	void setNote (int noteNum);
	
	/**
	 * sets the amplitude of the oscillator
     * @param amp amplitude to set oscillator to.
	 */
	void setAmplitude (float amp);
	
	/**
	 resets the oscillator
	 */
	void reset();
	
	/**
	 * sets the sample rate
     * @param sr sample rate value for oscillator.
	 */
	void setSampleRate (float sr);
	
	/**
	 Returns the next sample
     @return returns next sample.
	 */
	float nextSample();
	
	/**
	 * function that provides the execution of the waveshape
     * @param currentPhase current phase position to render shape for.
     * @return wave shape for current phase position.
	 */
	float renderWaveShape (const float currentPhase);
	
private:
	float frequency;
    float amplitude;
    float sampleRate;
	float phase;
    float phaseInc;
};

#endif //H_TRIOSCILLATOR