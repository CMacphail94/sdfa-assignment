//
//  Counter.h
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 02/01/2015.
//
//

#ifndef COUNTER_H_INCLUDED
#define COUNTER_H_INCLUDED

#include <iostream>
#include "../../JuceLibraryCode/JuceHeader.h"
/** Class for a Counter with its own Thread 
 *
 */
class Counter   :   public Thread
{
    
    
public:
    /**Class for counter listeners to inherit */
    class Listener
    {
    public:
        virtual ~Listener() {}
        
        virtual void counterChanged (const unsigned int counterValue) =0;
    };

    //==============================================================================
    /** Counter Constructor */
    Counter ();
    
    /** Counter Destructor */
    ~Counter();
    
    /** Run Counter */
    void run() override;
    
    /** set Counter Listener 
     * @param newListener what will now listen to counter for changes
     */
    void setListener(Listener* newListener);
    
    /** set Counter Tempo 
     * @param newTempo new value for tempo
     */
    void setTempo(int newTempo);
    
    private:
    Listener* listener;
    int tempo;
    


    
};


#endif /* COUNTER_H_INCLUDED */
