/*
 *  SawOscillator.h
 *
 *
 *
 *
 */


#ifndef H_SAWOSCILLATOR
#define H_SAWOSCILLATOR

#include "../JuceLibraryCode/JuceHeader.h"

/**
 * Class for a sawtooth wave oscillator
 * @see SquareOscillator
 * @see TriOscillator
 */

class SawOscillator
{
public:
	//==============================================================================
	/**
	 SawOscillator constructor
	 */
	SawOscillator();
	
	/**
	 SawOscillator destructor
	 */
	~SawOscillator();
	
	/**
	 sets the frequency of the oscillator
	 */
	void setFrequency (float freq);
	
	/**
	 sets frequency using a midi note number
	 */
	void setNote (int noteNum);
	
	/**
	 sets the amplitude of the oscillator
	 */
	void setAmplitude (float amp);
	
	/**
	 resets the oscillator
	 */
	void reset();
	
	/**
	 sets the sample rate
	 */
	void setSampleRate (float sr);
	
	/**
	 Returns the next sample
	 */
	float nextSample();
	
	/**
	 function that provides the execution of the waveshape
	 */
	float renderWaveShape (const float currentPhase);
	
private:
	float frequency;
    float amplitude;
    float sampleRate;
	float phase;
    float phaseInc;
};

#endif //H_TRIOSCILLATOR