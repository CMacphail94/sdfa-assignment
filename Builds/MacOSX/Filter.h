//
//  Filter.h
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 20/01/2015.
//
//

#ifndef H_FILTER
#define H_FILTER

#include "../JuceLibraryCode/JuceHeader.h"

/**
 * Class for a filter
 * @see FilterUI
 */
class Filter : public Component

{
public:
    /** Filter Constructor*/
    Filter();
    
    /** Filter Destructor*/
    ~Filter();
    
    /** Provides filter processing on a specified sample when called 
     * @param Sample sample to be processed
     * @return processed sample
     */
    float Process(float Sample);
    
    /** Sets filter cutoff frequency 
     * @param freq new cutoff frequency
     */
    void setCutoff(float freq);
    
    /** Sets filter type
     * @param type filter type where High Pass = 1, Low Pass = 0
     */
    void setFilterType(int type);
    
    /** Set Bypass State
     * @param bypass true or false boolean value
     */
    void setBypassState(bool bypass);
    
    /** Returns the bypass state when called
     * @return bypass state boolean value
     */
    bool getBypassState();
    
private:
    IIRFilter mainFilter;
    int filterType;
    float currentFreq;
    bool bypassState;
    
};


#endif //H_SEQUENCE