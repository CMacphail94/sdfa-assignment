//
//  Counter.cpp
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 02/01/2015.
//
//

#include "Counter.h"

Counter::Counter() : Thread ("CounterThread")
{
    listener = nullptr;
    tempo = 100;
}

Counter::~Counter()
{
    
}

void Counter::run()
{
    uint32 count = 0;
    while(!threadShouldExit())
    {
        uint32 time = Time::getMillisecondCounter();
        if(listener != nullptr)
        {
            if(count < 7)
                listener->counterChanged(count++);
            else
            {
                
                listener->counterChanged(count++);
                count = 0;
            }
        }
        Time::waitForMillisecondCounter(time + tempo);
    }
 
}

void Counter::setListener(Listener* newListener)
{
    listener = newListener;
}

void Counter::setTempo(int newTempo)
{
    tempo = newTempo; 
}