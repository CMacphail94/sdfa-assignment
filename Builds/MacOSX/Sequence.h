//
//  Sequence.h
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 29/12/2014.
//
//


#ifndef H_SEQUENCE
#define H_SEQUENCE

#include "../JuceLibraryCode/JuceHeader.h"

/**
 * Class for a sequencer
 * @see MainComponent
 */
class Sequence : public Component,
                public Button::Listener,
                public Slider::Listener,
                public ComboBox::Listener

{
public:
    /** Sequencer Constructor */
    Sequence();
    
    /** Sequencer Destructor */
    ~Sequence();
    
    /** Sequencer Ui element resized */
    void resized() override;
    
    /** Sequencer Ui element paint 
     * @param g refers to graphics object
     */
    void paint (Graphics& g) override;
    
    /** Sequencer Ui button clicked 
     * @param button refers to button clicked
     */
    void buttonClicked (Button* button) override;
    
    /** Sequencer Ui slider changed 
     * @param slider refers to slider changed
     */
    void sliderValueChanged (Slider* slider) override;
    
    /** Sequencer Ui combo Box Changed 
     * @param comboBox refers to combo box changed
     */
    void comboBoxChanged (ComboBox *comboBox) override;
    
    /** Returns the frequency for the current step in sequence 
     * @param step integer value of current step
     * @return integer value for current step frequency
     */
    int getStepFreq(int step);
    
    /** Returns the amplitude for the current step in sequence 
     * @param step integer value for current step
     * @return floating point value for current step amplitude
     */
    float getStepAmp(int step);
    
    /** Returns the numerical value of the current step 
     * @param step integer value for current step
     * @return integer value for current step
     */
    int getStep(int step);
    

    
private:
    TextButton seqButton[8];
    Slider seqVolSlider;
    Slider ampSlider[8];
    int seqValues[8];
    float seqAmps[8];
    int currentStep;
    ComboBox rootKeySelect;
    float rootKeyValue;
    Label sequencerLabel;
    
    
    
    
    

    
};


#endif //H_SEQUENCE