//
//  Filter.cpp
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 20/01/2015.
//
//

#include "Filter.h"

Filter::Filter()
{
    IIRFilter();
    mainFilter.setCoefficients(juce::IIRCoefficients::makeLowPass(44100,20000));
    filterType = 0; // low pass = 0 - hi pass = 1
}

Filter::~Filter()
{
    
}

float Filter::Process(float Sample)
{
    return mainFilter.processSingleSampleRaw(Sample);
}

void Filter::setCutoff(float freq)
{
    if(filterType == 0)
        mainFilter.setCoefficients(juce::IIRCoefficients::makeLowPass(44100,freq));
    else
        mainFilter.setCoefficients(juce::IIRCoefficients::makeHighPass(44100,freq));
    
    currentFreq = freq;
}

void Filter::setFilterType(int type)
{
    if(filterType != type)
    {
        filterType = type;
        if(filterType == 0)
            mainFilter.setCoefficients(juce::IIRCoefficients::makeLowPass(44100,currentFreq));
        else
            mainFilter.setCoefficients(juce::IIRCoefficients::makeHighPass(44100,currentFreq));

        
    }
}

void Filter::setBypassState(bool bypass)
{
    bypassState = bypass;
}

bool Filter::getBypassState()
{
    return bypassState;
}