//
//  SquareOscillator.h
//  JuceBasicAudio
//
//  Created by Cameron MacPhail on 27/12/2014.
//
//

#ifndef H_SQUAREOSCILLATOR
#define H_SQUAREOSCILLATOR


#include <iostream>
#include "../JuceLibraryCode/JuceHeader.h"

/**
 * Class for a squarewave oscillator
 * @see TriOscillator
 * @see SawOscillator
 */

class SquareOscillator
{
public:
	//==============================================================================
	/**
	 SawOscillator constructor
	 */
	SquareOscillator();
	
	/**
	 SawOscillator destructor
	 */
	~SquareOscillator();
	
	/**
	 sets the frequency of the oscillator
	 */
	void setFrequency (float freq);
	
	/**
	 sets frequency using a midi note number
	 */
	void setNote (int noteNum);
	
	/**
	 sets the amplitude of the oscillator
	 */
	void setAmplitude (float amp);
	
	/**
	 resets the oscillator
	 */
	void reset();
	
	/**
	 sets the sample rate
	 */
	void setSampleRate (float sr);
	
	/**
	 Returns the next sample
	 */
	float nextSample();
	
	/**
	 function that provides the execution of the waveshape
	 */
	float renderWaveShape (const float currentPhase);
	
private:
	float frequency;
    float amplitude;
    float sampleRate;
	float phase;
    float phaseInc;
};

#endif //H_SQUAREOSCILLATOR

